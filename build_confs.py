# -*- coding: utf-8 -*-

import re
import time


# confs names in template/ and ../
# except sr_head and sr_foot

def getRulesStringFromFile(path):
    file = open(path, 'r', encoding='utf-8')
    contents = file.readlines()
    ret = ''

    for content in contents:
        content = content.strip('\r\n')
        if not len(content):
            continue

        if content.startswith('#'):
            ret += content + '\n'
        else:
            prefix = 'DOMAIN-SUFFIX'
            if re.match(r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}', content):
                prefix = 'IP-CIDR'
                if '/' not in content:
                    content += '/32'
            elif '.' not in content:
                prefix = 'DOMAIN-KEYWORD'

            ret += prefix + ',%s\n' % (content)

    return ret


# make values
values = {}

# values['build_time'] = time.strftime("%Y-%m-%d %H:%M:%S")

values['ad'] = getRulesStringFromFile('resultant/ad.list')


# make confs
template=values['ad']

file_output = open('resultant/adb.conf', 'w', encoding='utf-8')

file_output.write(template)
